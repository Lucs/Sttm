/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   proj.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 16:16:52 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:19:03 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROJ_H
# define PROJ_H
# include "libft.h"
# include "mlx.h"

# define WIN_X 1024
# define WIN_Y 1024
# define MAP_SIZE 32

typedef struct			s_way
{
	int			direction;
	int			pivot;
	struct s_way		*nxt;
}				t_way;

typedef struct			s_clist
{
	int			id;
	int			direction;
	int			dst;
	int			posx;
	int			posy;
	int			alive;
	int			parc;
	int			play;
	struct s_way		*dir;
	struct s_clist		*nxt;
}				t_clist;

typedef struct			s_mlx
{
	void			*mlx;
	void			*win;
	void			*img;
	char			*str_img;
	int			bpp;
	int			s_l;
	int			endn;
}				t_mlx;

typedef struct			s_gb
{
	int			cycles;
	int			**map;
	int			gen;
	int			visual;
	int			acceleration;
	int			pause;
	int			bx;
	int			by;
	int			togen;
	int			parc;
	struct s_way		*way;
	struct s_clist		*c;
	struct s_mlx		*s;
}				t_gb;

int				ft_init_win(t_gb *g);
int				**ft_init_map(int n);
void				ft_fill_pixel(t_mlx *s, int y, int x, int col);
void				print_base(t_gb *g);
int				ft_exit(t_gb *g);
int				ft_key(int keycode, t_gb *g);
void				ft_generate(t_gb *g);
int				ft_update_cycles(t_gb *g);
void				do_mission(t_gb *g, t_clist *c);
void				do_mission_wood(t_gb *g, t_clist *c);
void				do_mission_house(t_gb *g, t_clist *c);
void				do_mission_crop(t_gb *g, t_clist *c);
void				ft_generate_wood(t_gb *g, int n);
void				ft_generate_water(t_gb *g);
int				ft_generate_cityzen(t_gb *g, int y, int x);

#endif
