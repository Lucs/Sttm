/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 12:08:36 by lcharbon          #+#    #+#             */
/*   Updated: 2017/08/26 02:52:58 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char				*ft_get_mantisse(double a, int pre)
{
	char				*ret;
	int					i;

	i = 0;
	ret = ft_strnewch(pre, '0');
	while (--pre + 1 > 0)
	{
		a *= 10;
		ret[i++] = (int)a + 48;
		a -= (int)a;
	}
	if (a * 10 >= 5 && ret[i - 1])
	{
		ret[--i] += 1;
		while (i > 0)
		{
			if (ret[i] == 58)
			{
				ret[i] -= 10;
				ret[i - 1] += 1;
			}
			i--;
		}
	}
	return (ret);
}

char					*ft_ftoa(double n, int pre)
{
	long				t1;
	double				tmp;
	char				*mantisse;
	char				*ret;

	if (pre == -1)
		pre = 6;
	t1 = (long)n;
	if (pre == 0)
		return (ft_itoa(t1));
	tmp = n - (double)t1;
	if (tmp < 0)
		tmp *= -1;
	mantisse = ft_get_mantisse(tmp, pre);
	ret = ft_strjoindel(ft_itoa(t1), ".", 1);
	ret = ft_strjoindel(ret, mantisse, 3);
	return (ret);
}
