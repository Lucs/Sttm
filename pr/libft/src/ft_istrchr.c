/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_istrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 17:23:07 by lcharbon          #+#    #+#             */
/*   Updated: 2017/09/13 17:23:36 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					ft_istrchr(const char *s, int c)
{
	char			*a;
	unsigned char	e;
	size_t			i;

	a = (char*)s;
	e = c;
	i = 0;
	while (a[i] != '\0')
	{
		if (a[i] == e)
			return (1);
		i++;
	}
	if (a[i] == e)
		return (1);
	return (0);
}
