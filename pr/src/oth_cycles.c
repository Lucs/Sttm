
#include "proj.h"

static void			generate_base(t_gb *g)
{
	t_clist			*new;

	new = ft_memalloc(sizeof(t_clist));
	g->map[16][4] = 1;
	new->posx = 4;
	new->posy = 16;
	new->alive = 1;
	new->id = 1;
	new->direction = rand() % 4;
	new->nxt = NULL;
	g->c = new;
	g->gen = 1;
}

static void			execute(t_gb *g, t_clist *c)
{
	t_way			*tmp;
	int			a;

	if (!c->alive)
		return ;
	g->map[c->posy][c->posx] = 0;
	if (c->direction == 0)
		c->posx++;
	else if (c->direction == 1)
		c->posx--;
	else if (c->direction == 2)
		c->posy++;
	else if (c->direction == 3)
		c->posy--;
	if (c->posx < 0 || c->posy < 0 || c->posx >= 32 || c->posy >= 32 ||
	g->map[c->posy][c->posx] == 3)
	{
		if (c->direction == 0)
			c->posx--;
		else if (c->direction == 1)
			c->posx++;
		else if (c->direction == 2)
			c->posy--;
		else if (c->direction == 3)
			c->posy++;
		a = c->direction;
		c->direction = rand() % 2;
		if (a == 0 || a == 1 || g->c->direction == a)
			g->c->direction += 2;
	}
	if (c->alive)
		g->map[c->posy][c->posx] = 1;
	c->parc++;
}

int				ft_update_cycles(t_gb *g)
{
	int		a;

	a = 0;
	srand(time(NULL));
	if (g->pause)
		return (1);
	ft_printf("Nombre de cycles : %d\n", g->cycles++);
	if (g->gen == 0)
		generate_base(g);
	a = g->c->direction;
	if (rand() % 5 == 0)
	{
		g->c->direction = rand() % 2;
		if (a == 0 || a == 1 || g->c->direction == a)
			g->c->direction += 2;
	}
	execute(g, g->c);
	if (g->visual)
		print_base(g);
	return (1);
}
