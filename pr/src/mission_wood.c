/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mission_wood.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 12:03:56 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:29:37 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

static int		find_dst(t_gb *g, t_clist *c, int posx, int posy)
{
	int			x;
	int			y;
	int			beha;
	int			loop;

	x = posx;
	y = posy;
	beha = 1;
	loop = 1;
	while (loop)
	{
		y = posy - beha - 1;
		while (loop && ++y < posy + beha)
		{
			x = posx - beha - 1;
			while (loop && ++x < posx + beha)
			{
				if (y >= 0 && x >= 0 && y < 256 && x < 256 && g->map[y][x] == WOOD)
					loop = 0;
			}
		}
		beha++;
	}
	c->dstx = x;
	c->dsty = y;
	g->map[y][x] = WOOD_NO;
	return (1);
}

void			do_mission_wood(t_gb *g, t_clist *c)
{
	if ((g->map[c->dsty][c->dstx] != WOOD && g->map[c->dsty][c->dstx] != WOOD_NO) || (c->dstx == 0 && c->dsty == 0))
		if (!find_dst(g, c, c->posx, c->posy))
			c->mission = MISSION_HOUSE;
	if (c->dstx - c->posx > 1)
		c->posx++;
	else if (c->dstx - c->posx < -1)
		c->posx--;
	if (c->dsty - c->posy > 1)
		c->posy++;
	else if (c->dsty - c->posy < -1)
		c->posy--;
	if (ft_abs(c->dstx - c->posx + c->dsty - c->posy) <= 2)
		c->wood++;
	if (c->wood >= 10)
	{
		g->map[c->dsty][c->dstx] = 0;
		c->mission = MISSION_HOUSE;
	}
}
