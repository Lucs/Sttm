/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mission_house.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 12:06:59 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:29:45 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

static int		dst_house(t_gb *g, t_clist *c, int *upx, int *upy)
{
	*upx = c->house->house_x - c->posx;
	*upy = c->house->house_y - c->posy;
	return (1);
}

void			update_house(t_gb *g, t_clist *c, t_house *h)
{
	int			y;
	int			x;
	int			tail;

	tail = 0;
	if (h->house_w == 5)
		tail = 1;
	else if (h->house_w == 20)
		tail = 2;
	else if (h->house_w == 50)
		tail = 3;
	y = h->house_y - tail;
	while (++y < h->house_y + tail)
	{
		x = h->house_x - tail;
		while (++x < h->house_x + tail)
			if (y >= 0 && x >= 0 && y < 256 && x < 256)
				g->map[y][x] = HOUSE;
	}
}

static int		ft_is_neighbour(t_gb *g, int ym, int xm)
{
	int			x;
	int			y;

	y = ym - 3;
	while (++y <= ym + 2)
	{
		x = xm - 3;
		while (++x <= xm + 2)
			if (x < 0 || x >= 256 || y < 0 || y >= 256 || g->map[y][x] == WATER ||
			g->map[y][x] == HOUSE || g->map[y][x] == HOUSE_N)
				return (0);
	}
	return (1);
}

static int		ft_can_print(char c)
{
	if (c == 0 || c == WOOD || c == WOOD_NO)
		return (1);
	return (0);
}

static void		ft_set_house(t_gb *g, int xm, int ym)
{
	int			x;
	int			y;

	y = ym - 4;
	while (++y <= ym + 3)
	{
		x = xm - 4;
		while (++x <= xm + 3)
			if (x >= 0 && x < 256 && y >= 0 && y < 256 && ft_can_print(g->map[y][x]) == 1)
				g->map[y][x] = HOUSE_N;
	}
}

void			do_mission_house(t_gb *g, t_clist *c)
{
	int			dstx;
	int			dsty;

	if (c->house == NULL)
	{
		g->house++;
		c->house = ft_memalloc(sizeof(t_house));
		c->house->house_x = rand() % 256;
		c->house->house_y = rand() % 256;
		while (ft_is_neighbour(g, c->house->house_y, c->house->house_x) != 1)
		{
			c->house->house_x = rand() % 256;
			c->house->house_y = rand() % 256;
		}
		ft_set_house(g, c->house->house_x, c->house->house_y);
	}
	dst_house(g, c, &dstx, &dsty);
	if (dstx > 1)
		c->posx++;
	else if (dstx < -1)
		c->posx--;
	if (dsty > 1)
		c->posy++;
	else if (dsty < -1)
		c->posy--;
	if (ft_abs(dstx + dsty) <= 2 && c->wood >= 10)
	{
		c->house->house_w++;
		c->wood = 0;
		c->mission = MISSION_WOOD;
		update_house(g, c, c->house);
	}
}
