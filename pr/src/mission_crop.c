/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mission_crop.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 12:15:33 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:26:37 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

static int		dst_crop(t_gb *g, t_clist *c, int *upx, int *upy)
{
	*upx = c->crop->crop_x - c->posx;
	*upy = c->crop->crop_y - c->posy;
	return (1);
}

void			update_crop(t_gb *g, t_clist *c, t_crop *h)
{
	int			y;
	int			x;

	if (c->crop->crop_w > 25)
	{
		y = h->crop_y - 4;
		while (++y < h->crop_y + 3)
		{
			x = h->crop_x - 4;
			while (++x < h->crop_x + 3)
				if (y >= 0 && x >= 0 && y < 256 && x < 256)
					g->map[y][x] = CROP;
		}
	}
}

static int		ft_is_neighbour(t_gb *g, int ym, int xm)
{
	int			x;
	int			y;

	y = ym - 5;
	while (++y <= ym + 3)
	{
		x = xm - 4;
		while (++x <= xm + 3)
			if (x < 0 || x >= 256 || y < 0 || y >= 256 || g->map[y][x] == WATER || g->map[y][x] == HOUSE || g->map[y][x] == HOUSE_N)
				return (0);
	}
	return (1);
}

static int		ft_can_print(char c)
{
	if (c == 0 || c == WOOD || c == WOOD_NO)
		return (1);
	return (0);
}

static void		ft_set_crop(t_gb *g, int xm, int ym)
{
	int			x;
	int			y;

	y = ym - 4;
	while (++y <= ym + 3)
	{
		x = xm - 4;
		while (++x <= xm + 3)
			if (x >= 0 && x < 256 && y >= 0 && y < 256 && ft_can_print(g->map[y][x]) == 1)
				g->map[y][x] = CROP_N;
	}
}

void			do_mission_crop(t_gb *g, t_clist *c)
{
	int			dstx;
	int			dsty;

	if (c->crop == NULL)
	{
		c->crop = ft_memalloc(sizeof(t_crop));
		c->crop->crop_x = rand() % 256;
		c->crop->crop_y = rand() % 256;
		while (ft_is_neighbour(g, c->crop->crop_y, c->crop->crop_x) != 1)
		{
			c->crop->crop_x = rand() % 256;
			c->crop->crop_y = rand() % 256;
		}
		ft_set_crop(g, c->crop->crop_x, c->crop->crop_y);
	}
	dst_crop(g, c, &dstx, &dsty);
	if (dstx > 1)
		c->posx++;
	else if (dstx < -1)
		c->posx--;
	if (dsty > 1)
		c->posy++;
	else if (dsty < -1)
		c->posy--;
	if (ft_abs(dstx + dsty) <= 2 && c->wood >= 10)
	{
		c->crop->crop_w++;
		c->mission = NO_MISSION;
		update_crop(g, c, c->crop);
	}
}
