/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/12 14:54:42 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 11:57:09 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

int		**ft_init_map(int n)
{
	int	**map;
	int	i;

	i = -1;
	if (!(map = ft_memalloc(sizeof(int*) * (MAP_SIZE + 1))))
		return (NULL);
	map[MAP_SIZE] = NULL;
	while (++i < MAP_SIZE)
	{
		if (!(map[i] = ft_memalloc(sizeof(int) * (MAP_SIZE + 1))))
			return (NULL);
		map[i][MAP_SIZE] = 0;
		if (n == 1)
			ft_memset(map[i], -1, MAP_SIZE * sizeof(int));
	}
	return (map);
}
