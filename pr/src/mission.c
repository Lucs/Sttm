/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mission.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 12:05:08 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:29:31 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

void			do_mission(t_gb *g, t_clist *c)
{
	g->mappos[c->posy][c->posx] = NOTHING;
	if (!c->mission)
	{
		if (c->wood < 10)
			c->mission = MISSION_WOOD;
		else
			c->mission = MISSION_HOUSE;
	}
	if (c->mission == MISSION_WOOD)
		do_mission_wood(g, c);
	else
		do_mission_house(g, c);
	g->mappos[c->posy][c->posx] = CITYZEN;
}

