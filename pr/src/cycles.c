/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cycles.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 11:01:51 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:07:21 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

static void			ft_free_all_bis(t_way *way)
{
	t_way			*tmp;

	while (way)
	{
		tmp = way->nxt;
		free(way);
		way = tmp;
	}
	way = NULL;
	tmp = NULL;
}

static void			ft_free_all(t_gb *g)
{
	t_clist			*tmp;

	while (g->c)
	{
		tmp = g->c->nxt;
		ft_free_all_bis(g->c->dir);
		free(g->c);
		g->c = tmp;
	}
	g->c = NULL;
	tmp = NULL;
}

static void			generate_base(t_gb *g)
{
	int			i;
	t_clist			*new;
	t_way			*wnew;

	i = 0;
	while (i < 10)
	{
		new = ft_memalloc(sizeof(t_clist));
		wnew = ft_memalloc(sizeof(t_way));
		wnew->nxt = NULL;
		g->map[16][4] = 1;
		new->posx = 4;
		new->posy = 16;
		new->alive = 1;
		new->id = 10 - i;
		wnew->direction = rand() % 4;
		new->direction = wnew->direction;
		new->dir = wnew;
		new->nxt = g->c;
		g->c = new;
		i++;
	}
	g->gen = 1;
}

static void			generate_new(t_gb *g)
{
	int			i;
	t_clist			*new;
	t_way			*wnew;
	t_way			*tmp;

	i = 0;
	while (i < 10)
	{
		new = ft_memalloc(sizeof(t_clist));
		wnew = ft_memalloc(sizeof(t_way));
		wnew->nxt = NULL;
		g->map[16][4] = 1;
		new->posx = 4;
		new->posy = 16;
		new->alive = 1;
		new->id = 10 - i;
		wnew->direction = rand() % 4;
		new->nxt = g->c;
		g->c = new;
		i++;
	}
	tmp = g->way;
	while (tmp->nxt)
		tmp = tmp->nxt;
	tmp->pivot = rand() % g->parc;
	tmp->nxt = wnew;
	new->dir = g->way;
	new->direction = new->dir->direction;
	g->togen = 0;
}

static int			save_dst(t_gb *g, t_clist *c)
{
	return (ft_abs(c->posy - 16) + ft_abs(c->posx - 30));
}

static t_way			*ft_new_way(t_gb *g, t_way *dir)
{
	t_way			*new;
	t_way			*tmp;

	new = ft_memalloc(sizeof(t_way));
	ft_memcpy(new, dir, sizeof(t_way));
	tmp = g->way;
	if (!tmp)
		return (new);
	while (tmp->nxt)
		tmp = tmp->nxt;
	tmp->nxt = new;
	return (g->way);
}

static void			ft_get_best(t_gb *g)
{
	t_clist		*tmp;
	int		best;

	best = 999999;
	tmp = g->c;
	while (tmp)
	{
		if (tmp->dst < best)
			best = tmp->dst;
		tmp = tmp->nxt;
	}
	tmp = g->c;
	while (tmp && tmp->dst != best)
		tmp = tmp->nxt;
	g->way = ft_new_way(g, tmp->dir);
	g->parc = tmp->parc;
}

static void			execute(t_gb *g, t_clist *c)
{
	t_way			*tmp;
	if (!c->alive)
		return ;
	g->map[c->posy][c->posx] = 0;
	if (c->direction == 0)
		c->posx++;
	else if (c->direction == 1)
		c->posx--;
	else if (c->direction == 2)
		c->posy++;
	else if (c->direction == 3)
		c->posy--;
	if (c->posx < 0 || c->posy < 0 || c->posx >= 32 || c->posy >= 32 ||
	g->map[c->posy][c->posx] == 3)
	{
		c->dst = save_dst(g, c);
		c->alive = 0;
	}
	if (c->alive)
		g->map[c->posy][c->posx] = 1;
	c->parc++;
	if (c->dir && c->play == c->dir->pivot)
	{
		tmp = c->dir->nxt;
	//	free(c->dir);
		c->dir = tmp;
		c->play = 0;
		c->direction = c->dir->direction;
	}
}

int				ft_update_cycles(t_gb *g)
{
	t_clist		*tmp;
	int		a;

	a = 0;
	srand(time(NULL));
	if (g->pause)
		return (1);
	ft_printf("Nombre de cycles : %d\n", g->cycles++);
	if (g->gen == 0)
		generate_base(g);
	if (g->togen == 1)
		generate_new(g);
	tmp = g->c;
	while (tmp)
	{
		if (tmp->alive == 0)
			a++;
		tmp->play++;
		execute(g, tmp);
		tmp = tmp->nxt;
	}
	if (a == 10)
	{
		ft_putendl("Ici");
		ft_get_best(g);
		ft_free_all(g);
		g->togen = 1;
	}
	if (g->visual)
		print_base(g);
	return (1);
}
