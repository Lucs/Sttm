/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <lcharbon@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/31 05:39:22 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 12:27:21 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

static void		ft_fill_pixel_inter(t_mlx *s, int y, int x, int pos)
{
	if (pos == 0)
		ft_fill_pixel(s, y, x, 0xAAAAAA);
	else if (pos == 1)
		ft_fill_pixel(s, y, x, 0xAA0000);
	else if (pos == 2)
		ft_fill_pixel(s, y, x, 0xAAAA00);
	else if (pos == 3)
		ft_fill_pixel(s, y, x, 0x000000);
}

static void		ft_print_square(t_gb *g, int **map, int ym, int xm)
{
	int			x;
	int			y;
	int			sq_lenx;
	int			sq_leny;
	int			rmy;
	int			rmx;
	unsigned char		line;

	rmx = xm;
	rmy = ym;
	line = 0;
	sq_lenx = WIN_X / MAP_SIZE;
	sq_leny = WIN_Y / MAP_SIZE;
	y = (sq_leny * ym) - 1;
	while (++y < (sq_leny * (ym + 1)))
	{
		x = (sq_lenx * xm) - 1;
		while (++x < (sq_lenx * (xm + 1)))
		{
			if (x == (sq_lenx * (xm + 1)) - line || y == (sq_leny * (ym + 1)) - line)
				ft_fill_pixel(g->s, y, x, 0x000000);
			else
				ft_fill_pixel_inter(g->s, y, x, map[rmy][rmx]);
		}
	}
}

void			print_base(t_gb *g)
{
	int			x;
	int			y;

	y = -1;
	while (++y < MAP_SIZE)
	{
		x = -1;
		while (++x < MAP_SIZE)
			ft_print_square(g, g->map, y, x);
	}
	y = -1;
	mlx_put_image_to_window(g->s->mlx, g->s->win, g->s->img, 0, 0);
}

