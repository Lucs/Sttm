/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/12 14:54:42 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 11:52:42 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

int			ft_init_win(t_gb *g)
{
	if (!(g->s->mlx = mlx_init()))
		return (0);
	if (!(g->s->win = mlx_new_window(g->s->mlx, WIN_X, WIN_Y, "ia_proj")))
		return (0);
	g->s->img = mlx_new_image(g->s->mlx, WIN_X, WIN_Y);
	g->s->str_img = mlx_get_data_addr(g->s->img,
	&g->s->bpp, &g->s->s_l, &g->s->endn);
	return (1);
}

int			ft_exit(t_gb *g)
{
	int		i;

	i = -1;
	while (++i < MAP_SIZE + 1)
		ft_memdel((void**)&g->map[i]);
	ft_memdel((void**)&g->map);
	mlx_destroy_image(g->s->mlx, g->s->img);
	mlx_destroy_window(g->s->mlx, g->s->win);
	ft_memdel((void**)&g->s);
	exit (0);
	return (1);
}

int			ft_key(int keycode, t_gb *g)
{
//	ft_putnbrendl(keycode);
	if (keycode == 53)
		ft_exit(g);
	else if (keycode == 6)
		g->visual++;
	else if (keycode == 0 && g->acceleration < 50)
		g->acceleration++;
	else if (keycode == 2 && g->acceleration > 1)
		g->acceleration--;
	else if (keycode == 35)
		g->pause++;
	else if (keycode == 8)
		ft_update_cycles(g);
	if (g->visual)
		print_base(g);
	if (g->pause == 2)
		g->pause = 0;
	if (g->visual == 2)
		g->visual = 0;
	return (1);
}

