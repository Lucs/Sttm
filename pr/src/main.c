/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcharbon <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/12 14:54:42 by lcharbon          #+#    #+#             */
/*   Updated: 2017/11/07 11:55:55 by lcharbon         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "proj.h"

int				utils_click(int k, int x, int y, t_gb *g)
{
	if (k == 1)
		g->map[y / MAP_SIZE][x / MAP_SIZE] = 3;
	print_base(g);
	return (1);
}

void				init_map_two(t_gb *g)
{
	g->map[16][30] = 2;
	g->map[8][25] = 3;
	g->map[9][25] = 3;
	g->map[10][25] = 3;
	g->map[11][25] = 3;
	g->map[12][25] = 3;
	g->map[13][25] = 3;
	g->map[14][25] = 3;
	g->map[15][25] = 3;
	g->map[16][25] = 3;
	g->map[17][25] = 3;
	g->map[18][25] = 3;
	g->map[19][25] = 3;
	g->map[20][25] = 3;
	g->map[21][25] = 3;
	g->map[22][25] = 3;
	g->bx = 30;
	g->by = 16;
	g->acceleration = 1;
}

int				main(int argc, char **argv)
{
	t_gb	*g;

	if (!(g = ft_memalloc(sizeof(t_gb))))
		ft_perror("Memory allocation failed");
	if (!(g->map = ft_init_map(0)))
		ft_exit(g);
	if (argc > 1 && ft_strstr(argv[1], "--visu"))
		g->visual = 1;
	if (g->visual)
	{
		if (!(g->s = ft_memalloc(sizeof(t_mlx))))
			ft_perror("Memory allocation failed");
		if (!(ft_init_win(g)))
			ft_perror("Initiation of video failed");
	}
	init_map_two(g);
//	if (!g->visual)
//	{
//		while (42)
//			ft_update_cycles(g);
//	}
	print_base(g);
	mlx_mouse_hook(g->s->win, utils_click, g);
	mlx_hook(g->s->win, 17, 0, ft_exit, g);
	mlx_hook(g->s->win, 2, 0, ft_key, g);
	mlx_loop_hook(g->s->mlx, ft_update_cycles, g);
	mlx_loop(g->s->mlx);
	return (1);
}
